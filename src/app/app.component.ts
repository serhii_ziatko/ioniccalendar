import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen, NativeStorage } from 'ionic-native';
import { SessionServices } from '../services/session.services';
import { CalendarServices } from '../services/calendar.services';
import { SyncServices } from '../services/sync.services';
import { MessageServices } from '../services/message.services';

import { TabsPage } from '../pages/tabs/tabs';
import { UserPage } from '../pages/user/user';

@Component({
  template: `<ion-nav [root]="rootPage" ></ion-nav>`,
  providers: [SessionServices, CalendarServices, SyncServices, MessageServices],
})

export class MyApp {
  public rootPage: any;
  constructor(
    private sessionService: SessionServices,
    private messageService: MessageServices,
    private platform: Platform,
   // private navCtrl: NavController,
  ) {}

  ngOnInit() {
    this.messageService.presentLoading();
    this.platform.ready().then(() => {
      NativeStorage.getItem('user')
      .then((data) => {
        this.rootPage = TabsPage;
        Splashscreen.hide();
      }, (error) => {
        this.rootPage = UserPage;
        Splashscreen.hide();
      });

      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
