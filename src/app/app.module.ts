import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { FormsModule }   from '@angular/forms';
import { MyApp } from './app.component';
import { SettingsPage } from '../pages/settings/settings';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SingUpPage } from '../pages/singUp/singUp';
import { LoginPage } from '../pages/login/login';
import { RestorePasswordPage } from '../pages/restorePassword/restorePassword';
import { UsersServices } from '../services/users.services';
import { MessageServices } from '../services/message.services';
import { SessionServices } from '../services/session.services';
import { CalendarServices } from '../services/calendar.services';
import { SyncServices } from '../services/sync.services';
import { DatePicker } from '../components/datepicker/datepicker.component';
import { EventsCard } from '../components/eventsCard/eventsCard.component';
import { UserPage } from '../pages/user/user';

@NgModule({
  declarations: [
    MyApp,
    SettingsPage,
    ContactPage,
    HomePage,
    TabsPage,
    SingUpPage,
    LoginPage,
    RestorePasswordPage,
    DatePicker,
    UserPage,
    EventsCard
  ],
  imports: [
    FormsModule,
    IonicModule.forRoot(MyApp, {}, {
     links: [
      { component: UserPage, name: 'User', segment: 'user' },
      { component: TabsPage, name: 'TabsPage', segment: 'main' },
    ]
  })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage,
    ContactPage,
    HomePage,
    TabsPage,
    SingUpPage,
    LoginPage,
    RestorePasswordPage,
    DatePicker,
    UserPage,
    EventsCard
  ],
  providers: [
    UsersServices,
    MessageServices,
    SessionServices,
    CalendarServices,
    SyncServices
  ]
})
export class AppModule {}
