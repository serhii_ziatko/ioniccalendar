
declare namespace App {

    interface newUser{
        username: string;
        email: string;
        password: string;
    }

    interface login{
        id: number;
        username: string;
        password: string;
    }

    interface restorePassword{
        email: string;
    }
    interface day{
        dayId: string;
        events: Array<string>;
        otherMonth: string;
    }
    interface user{
        id: number;
        username: string;
        email: string
    }
    interface newEvent{
        type: string;
        date: string;
        title: string;
        notes: string;
        location: string;
        UserId: string;
        photo: any
    }
    interface event{
        id: number;
        type: string;
        date: string;
        endDate: string;
        title: string;
        notes: string;
        location: string;
        UserId: string;
        Images: App.img;
    }
    
    interface eventModel{
        id: string;
        type: string;
        date: string;
        endDate: string;
        location: string;
        title: string;
        notes: string;  
    }
    
    interface showMonth{
        year: number;
        month: number;
        weeks: Array<any>;
        curDay: any; 
    }
    interface icons {
        birthday: string;
        lesson: string;
        meeting: string;
    }
    interface image {
        b64: string;
        blob: Blob;
    }
    interface img {
        EventId: string,
        id: number,
        images: string,
    }

}