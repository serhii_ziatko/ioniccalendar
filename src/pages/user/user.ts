import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { SingUpPage } from '../singUp/singUp';
import { RestorePasswordPage } from '../restorePassword/restorePassword';

@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage {
  tab1Root: any = LoginPage;
  tab2Root: any = SingUpPage;
  tab3Root: any = RestorePasswordPage;

  constructor(private navCtrl: NavController) {}
}
