import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { UsersServices } from '../../services/users.services';
import { MessageServices } from '../../services/message.services';
import { CustomValidators } from '../../validators/validators';

@Component({
  selector: 'page-singUp',
  templateUrl: 'singUp.html'
})

export class SingUpPage implements OnInit {
  public serverErrors = false ;
  public singupForm: FormGroup;
  public username: AbstractControl;
  public password: AbstractControl;
  public email: AbstractControl;

  constructor(
    private usersService: UsersServices,
    private messageService: MessageServices,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
  ) {}

  ngOnInit() {
    this.singupForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(2),
        Validators.maxLength(10), CustomValidators.validateUsername])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      email: ['', Validators.compose([Validators.required, CustomValidators.validateEmail])]
    });
    this.username = this.singupForm.controls['username'];
    this.password = this.singupForm.controls['password'];
    this.email = this.singupForm.controls['email'];
  }

  resetForm(): void {
    this.singupForm.reset();
  }

  singUpForm(): void {
    this.addUser(this.singupForm.value);
  }

  addUser(newUser: App.newUser): void {
    newUser.username = newUser.username.trim();
    this.usersService.create(newUser).subscribe(res => {
      this.messageService.toast('Sing up success', 'top', 3000);
      this.serverErrors = false;
      this.switchTabs();
      this.resetForm();
    }, res => {
      this.serverErrors = true;
    });
  }

  hideServerError(): void {
    this.serverErrors = false;
  }

  switchTabs(): void {
    this.navCtrl.parent.select(0);
  }
}
