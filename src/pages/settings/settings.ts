import { Component } from '@angular/core';
import { NavController, AlertController, Platform } from 'ionic-angular';
import { UserPage } from '../user/user';
import { SessionServices } from '../../services/session.services';
import { MessageServices } from '../../services/message.services';
import { SyncServices } from '../../services/sync.services';
import { Facebook, NativeStorage } from 'ionic-native';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  user: any;
  userReady: boolean = false;

  constructor(
    private navCtrl: NavController,
    private sessionServices: SessionServices,
    private syncServices: SyncServices,
    private messageServices: MessageServices,
    private alertCtrl: AlertController,
    private platform: Platform

  ) {}

  exit(): void {
    Facebook.getLoginStatus().then(res => {
      if (res.status === 'connected') {
        this.doFbLogout();
        return;
      }
      this.sessionServices.delUser();
    });
    this.navCtrl.parent.parent.setRoot(UserPage);
  }
  exitConfirm(): void {
    const confirm = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'Do you want to exit?',
      buttons: [
        {
          text: 'Disagree'
        },
        {
          text: 'Agree',
          handler: () => {
            this.exit();
          }
        }
      ]
    });
    confirm.present();
  }

  syncWithDeviceConfirm(): void {
    const confirm = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'Do you want to sync all event with device?',
      buttons: [
        {
          text: 'Disagree'
        },
        {
          text: 'Agree',
          handler: () => {
            const user = this.sessionServices.getUser();
            this.syncServices.getAllEvents(user).subscribe(events => {
              this.syncServices.syncAllEvents(JSON.parse(events.json())).then( () => {
                this.messageServices.toast('Success!', 'top', 3000);
              }, err => {
                console.error(err);
                this.messageServices.toast('Device error!', 'top', 3000);
              });
            }, err => {
              console.error(err);
              this.messageServices.toast('Server error!', 'top', 3000);
            });
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewCanEnter() {

    NativeStorage.getItem('user')
    .then((data) => {
      this.user = {
        name: data.name,
        gender: data.gender,
        picture: data.picture
      };
        this.userReady = true;
    }, (error) => {
      console.log(error);
    });

  }

  doFbLogout() {

    Facebook.logout()
    .then((response) => {
      // user logged out so we will remove him from the NativeStorage
      this.sessionServices.delUser();
      this.navCtrl.parent.parent.setRoot(UserPage);
    }, (error) => {
      console.log(error);
    });
  }
}
