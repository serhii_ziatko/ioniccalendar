import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Facebook } from 'ionic-native';
import { Validators, FormBuilder, FormGroup, AbstractControl} from '@angular/forms';
import { CustomValidators } from '../../validators/validators';
import { UsersServices } from '../../services/users.services';
import { MessageServices } from '../../services/message.services';
import { SessionServices } from '../../services/session.services';
import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit {
  public FB_APP_ID: number = 233274763771597;
  public serverErrors: boolean = false;
  public loginForm: FormGroup;
  public username: AbstractControl;
  public password: AbstractControl;
  constructor(
    private usersService: UsersServices,
    private messageService: MessageServices,
    private sessionService: SessionServices,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required,
        Validators.minLength(2), Validators.maxLength(10),
        CustomValidators.validateUsername])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });

    this.username = this.loginForm.controls['username'];
    this.password = this.loginForm.controls['password'];
    Facebook.browserInit(233274763771597);
  }

  resetForm(): void {
    this.loginForm.reset();
  }

  goToCalendar(): void {
    this.navCtrl.parent.parent.setRoot(TabsPage);
  }

  logForm(newUser: App.login): void {
    this.messageService.presentLoading();
    this.usersService.login(newUser).subscribe(res => {
      this.sessionService.setUser(res);
      this.hideServerError();
      this.goToCalendar();
      this.resetForm();
      this.messageService.toast('Login up success', 'top', 3000);
    }, err => {
      console.error(err);
      this.serverErrors = true;
      this.messageService.toast('Login error!', 'top', 3000);
    });
  }

  hideServerError(): void {
    this.serverErrors = false;
  }



  doFbLogin() {
    this.messageService.presentLoading();
    let permissions = new Array();
    // the permissions your facebook app needs from the user
    permissions = ['public_profile', 'email'];
    Facebook.login(permissions)
    .then((response) => {
      let userId = response.authResponse.userID;
      let params = new Array();
      // Getting name and gender properties
      Facebook.api('/me?fields=name,gender,email', params)
      .then((user) => {
        user.picture = 'https://graph.facebook.com/' + userId + '/picture?type=large';
        this.usersService.create({
          username: user.name,
          email: user.email,
          password: user.userId
        }).subscribe(res => {
          // now we have the users info, let's save it in the NativeStorage
          this.sessionService.setUser({
            id: res[0].id,
            name: user.name,
            email: user.email
          }).then(() => {
            this.navCtrl.parent.parent.setRoot(TabsPage);
            this.messageService.toast('Login up success', 'top', 3000);
          },  (error) => {
            console.log(error);
          });
        }, err => { console.log(err); } );
      });
    }, (error) => {
      console.log(error);
    });
  }
}
