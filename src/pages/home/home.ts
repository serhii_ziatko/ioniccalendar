

import { Component, ViewChild } from '@angular/core';
import { NavController, Content} from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild(Content) content: Content;
  constructor(private navCtrl: NavController) {}

  scrollToTopForm() {
    const el = document.getElementById('topForms').offsetTop - 6;
    console.log(el);
    this.content.scrollTo(0, el, 300);
  }
}
