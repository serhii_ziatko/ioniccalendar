import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MessageServices } from '../../services/message.services';
import { LoginPage } from '../login/login';
import { CustomValidators } from '../../validators/validators';
import { UsersServices } from '../../services/users.services';

@Component({
  selector: 'page-registration',
  templateUrl: 'restorePassword.html',
})
export class RestorePasswordPage implements OnInit {
  public restorePasswordForm: FormGroup;
  public isEmailSend: boolean;
  public error = {
    emailNotExist: false
  };

  constructor(
    private usersService: UsersServices,
    private messageService: MessageServices,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController
  ) {}

  ngOnInit() {
    this.restorePasswordForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, CustomValidators.validateEmail])]
    });
  }

  resetForm(): void {
    this.restorePasswordForm.reset();
  }
  goToSingUp(): void {
    this.navCtrl.parent.select(0);
  }
  restorePassForm(): void {
    this.sendEmail(this.restorePasswordForm.value);
  }

  goToLoginPage(): void {
    this.navCtrl.push(LoginPage);
  }

  sendEmail(data: App.restorePassword): void {
    data.email = data.email.trim();
    this.usersService.sendEmail(data).subscribe(res => {
      this.messageService.toast('Password was send on your email:)', 'top', 3000);
      this.hideServerError();
      this.goToLoginPage();
      this.resetForm();
    }, err => {
      this.error.emailNotExist = true;
      this.messageService.toast('Error!', 'top', 3000);
    });
  }
  hideServerError(): void {
    this.error.emailNotExist = false;
  }
}
