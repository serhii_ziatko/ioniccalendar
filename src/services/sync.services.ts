import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Calendar } from 'ionic-native';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class SyncServices  {
  public userUrl = 'http://10.10.54.227:3000/';

  constructor(
    private http: Http,
    private platform: Platform,
  ) {}

  getAllEvents(user) {
    return this.http
      .post(this.userUrl + 'events/all/', user);
  }

  createOneEvent(event: App.event): Promise<any> {
    return this.platform.ready().then(() => {
      Calendar.createEvent(event.title, event.location, event.notes, new Date(event.date), new Date(event.endDate) );
    });
  }

  deleteOneEvent(event: App.event): Promise<any> {
    return this.platform.ready().then(() => {
      Calendar.deleteEvent(event.title, event.location, event.notes, new Date(event.date), new Date(event.endDate) );
    });
  }

  syncAllEvents(events: Array<App.event>): Promise<any> {
    return this.platform.ready().then(() => {
      events.forEach( item => {
        Calendar.createEvent(
          item.title,
          item.location,
          item.notes,
          new Date(item.date),
          new Date(item.endDate) );
      });
    });
  }
}
