import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UsersServices {
  public userUrl = 'http://10.10.54.227:3000/';
  constructor(
    private http: Http
  ) {}

  create(data: App.newUser) {
    return this.http.post(this.userUrl + 'singup/', data)
    .map(res => res.json());
  }
  login(data: App.login ) {
    return this.http.post(this.userUrl + 'login/', data)
    .map(res => res.json());
  }
  sendEmail(data: App.restorePassword) {
    return this.http.post(this.userUrl + 'restorepassword/', data)
    .map(res => res.json());
  }
}
