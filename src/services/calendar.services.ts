import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';
import { SessionServices } from './session.services';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CalendarServices  {
  public userUrl = 'http://10.10.54.227:3000/';
  public _eventsStore: Subject<{}> = new Subject();
  public events$ = this._eventsStore.asObservable();
  public _dayEvents: Subject<{}> = new Subject();
  public day$ = this._dayEvents.asObservable();
  public _selectDay: Subject<{}> = new Subject();
  public selectDay$ = this._selectDay.asObservable();
  public events: Array<any> = [];
  constructor(
    private http: Http,
    private sessionServices: SessionServices
  ) {}

  // get events for three months: current, previous, next.
  getEventsThreeMonth(data): Promise<any> {
    return this.sessionServices.getUser().then(user => {
      data.UserId = user.id;
      return this.http
      .post(this.userUrl + 'events/', data)
      .toPromise()
      .then(res => {
        this.events = res.json();
        console.log(this.events);
        this._eventsStore.next(res.json());
      }, err => console.error(err));
    }, err => console.error(err));

  }
  // get event for check day or current
  getDayEvents (day): void {
    this._selectDay.next(day);
    this._dayEvents.next(this.events.reduce((data, event) => {
      moment(event.date).format('DDMMYY') ===
      day.format('DDMMYY') && data.push(event);
      return data;
    }, [] ));
  }
  // create new event 
  create(data) {
    return this.http.post(this.userUrl + 'events/new-event/', data);
  }
  // remove event
  remove(id: number) {
    return this.http.delete(this.userUrl + `events/${id}`);
  }
  // edit event
  update(id: number, event: FormData) {
    return this.http.put(this.userUrl + `events/${id}`, event);
  }

}
