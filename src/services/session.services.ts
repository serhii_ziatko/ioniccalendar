import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { NativeStorage } from 'ionic-native';

@Injectable()
export class SessionServices {
  constructor(private http: Http) {}
  setUser(user): Promise<any> {
    return NativeStorage.setItem('user', user);
  }
  delUser(): Promise<any> {
    return NativeStorage.remove('user');
  }
  getUser(): Promise<any> {
    return NativeStorage.getItem('user');
  }

}
