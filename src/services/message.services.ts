import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';


@Injectable()
export class MessageServices {
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController
  ) {}

  toast(message: string, position: string, duration: number): void {
    const toast = this.toastCtrl.create({
      message,
      duration,
      position,
      });
    toast.present();
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 2000
    });
    loader.present();
  }
}
