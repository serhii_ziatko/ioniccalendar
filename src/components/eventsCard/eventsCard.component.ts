import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavController, ActionSheetController, AlertController } from 'ionic-angular';
import * as moment from 'moment';
import { CalendarServices } from '../../services/calendar.services';
import { SessionServices } from '../../services/session.services';
import { MessageServices } from '../../services/message.services';
import { SyncServices } from '../../services/sync.services';
import { Validators, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { CustomValidators } from '../../validators/validators';
import { Camera } from 'ionic-native';

@Component({
  selector: 'events-card',
  templateUrl: 'eventsCard.component.html'
})
export class EventsCard implements OnInit {
  @Output()scrollToTopForm: EventEmitter<any> = new EventEmitter();
  public dayEvents: Array<any> = [];
  public disabled: boolean = false;
  public disabledEdit: boolean = false;
  public showFormAddEvent: boolean = false;
  public showFormEditEvent: boolean = false;
  public toSync: boolean = false;
  public addEventModel: App.eventModel = {
    id: null,
    type: null,
    date: null,
    endDate: null,
    location: null,
    title: null,
    notes: null
  };
  public editEventModel: App.event = {
    id: null,
    type: null,
    date: null,
    endDate: null,
    location: null,
    title: null,
    notes: null,
    UserId: null,
    Images: null,
  };
  public imgUrl: string = 'assets/icon/City-No-Camera-icon.png';
  public image: App.image = {
    b64: null,
    blob: null
  };
  public addEventForm: FormGroup;
  public editEventForm: FormGroup;
  public title: AbstractControl;
  public titleEdit: AbstractControl;
  public endDate: AbstractControl;
  constructor(
    private navCtrl: NavController,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private calendarServices: CalendarServices,
    private sessionServices: SessionServices,
    private messageServices: MessageServices,
    private syncServices: SyncServices,
    private formBuilder: FormBuilder,
    // private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.calendarServices.day$.subscribe(arr => {
      this.dayEvents = arr as Array<any>;
    });
    this.calendarServices.selectDay$.subscribe(date => {
      this.addEventModel.date = this.addEventModel.endDate = moment(date).format() as string;
      this.editEventModel.date = this.editEventModel.endDate = moment(date).format() as string;
    });
    this.editEventForm = this.addEventForm = this.formBuilder.group({
      type: ['', Validators.required],
      date: ['', Validators.required],
      endDate: ['', Validators.required],
      location: [''],
      title: ['', Validators.required],
      notes: [''],

    }, { validator: CustomValidators.checkEndDate('date', 'endDate') } );
    this.title = this.addEventForm.controls[ 'title' ];
    this.titleEdit = this.editEventForm.controls[ 'title' ];
  }

  // add event block
  // show form and get default values
  addEvent(day): void {
    this.deleteUploadPhoto();
    this.getEventForAdd(day);
    this.toSync = false;
    this.showFormAddEvent = true;
    this.scrollToTopForm.emit(null);
  }
  // get default values ( selected date and birthday type );
  getEventForAdd(day: App.day): void {
    this.addEventModel.type = 'birthday';
    this.addEventModel.date = this.addEventModel.endDate = moment(day).format();
  }
  // submit form and sync with device
  addEventFormSubmit(): void {
    this.disabled = true;
    const value = this.addEventForm.value;
    this.sessionServices.getUser().then( user => {
      value.UserId = user.id;

      let form = new FormData();
      Object.keys(value).forEach((item) => {
        if (value[item] === null) value[item] = '';
        form.append(item, value[item]);
      });
      if (this.image.blob) {
        form.append('images', this.image.blob);
      }
      this.calendarServices.create(form).subscribe(res => {
        this.disabled = false;
        this.calendarServices.getEventsThreeMonth({
          date: this.addEventModel.date
        });
        this.closeAddEventForm();
        this.deleteUploadPhoto();
      }, err => {
        this.disabled = false;
        console.error(err);
        return;
      });
    });

    if (this.toSync) {
      this.syncServices.createOneEvent(value).then( res => {
        return this.calendarServices.create(value);
      }, err => {
        console.error(err);
      });
    }
  }
  // close add event form and reset
  closeAddEventForm(): void {
    this.showFormAddEvent = false;
    this.addEventForm.reset();
    this.deleteUploadPhoto();
  }

  // edit block 
  // show form edit event and get values of current event  
  editEvent(event: App.event): void {
    this.getEventForEdit(event);
    this.showFormEditEvent = true;
    this.scrollToTopForm.emit(null);
  }
  // get values of current event
  getEventForEdit(event: App.event): void {
    console.log(event);
    this.editEventModel = event;
    this.editEventModel.date = moment(this.editEventModel.date).format();
    this.editEventModel.endDate = moment(this.editEventModel.endDate).format();
    if (event.Images[0] && event.Images[0].images) {
      this.image.b64 = event.Images[0].images;
    }
  }
  // submit form edit event
  editEventFormSubmit(): void {
    this.disabledEdit = true;
    const editedEvent = this.editEventModel;
    const value = this.editEventForm.value;

    Object.keys(value).forEach((item) => {
      editedEvent[item] = value[item];
    });

    let form = new FormData();

    Object.keys(editedEvent).forEach((item) => {
      if (item === 'Images') {
        if (!this.image.b64) {
          form.append(item, '[]');
          return;
        };
        const jsonStr = JSON.stringify(editedEvent[item]);
        form.append(item, jsonStr);
        return;
      }
      if (editedEvent[item] === null) {
        editedEvent[item] = '';
      }
      form.append(item, editedEvent[item]);
    });
    if (this.image.blob) {
      form.append('images', this.image.blob);
    }

    this.calendarServices.update(editedEvent.id, form).subscribe(res => {
      this.disabledEdit = false;
      this.calendarServices.getEventsThreeMonth({
        date: this.editEventModel.date,
      });
      this.closeEditEventForm();
      this.deleteUploadPhoto();
    }, err => {
      this.disabledEdit = false;
      console.error('Error');
    });
  }
  // close form edit event
  closeEditEventForm(): void {
    this.showFormEditEvent = false;
  }

  // delete block 
  // delete event with sync with device
  deleteEvent(event: App.event): void {
    const alert = this.alertCtrl.create();
      alert.setTitle('Are you sure?');
      alert.setMessage('Do you want delete event?');
      alert.addInput({
        type: 'checkbox',
        label: 'Delete from phone?',
        value: 'true',
      });
      alert.addButton('Disagree');
      alert.addButton({
        text: 'Agree',
        handler: data => {
          this.calendarServices.remove(event.id).subscribe(result => {
            this.calendarServices.getEventsThreeMonth({
              date: this.addEventModel.date,
            });
            this.messageServices.toast('Event was deleted!', 'top', 3000);
          }, err => {
            console.error('Error!');
            this.messageServices.toast('Error!', 'top', 3000);
            return;
          });
          if (data.length) {
            this.syncServices.deleteOneEvent(event).then( res => {
              return this.calendarServices.remove(event.id);
            }, err => {
              console.error('Device Error');
            });
          }
        }
      });
    alert.present();
  }

  // get time from date
  getTime(date: string): string {
    return moment(date).format('HH:mm');
  }

  // photo block
  // get photo from gallery
  openGallery (): void {
    const cameraOptions = {
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: Camera.DestinationType.DATA_URL,
      quality: 100,
      encodingType: Camera.EncodingType.JPEG,
      correctOrientation: true
    };
    Camera.getPicture(cameraOptions)
      .then((file_uri) => {
        this.image.b64 = 'data:image/jpeg;base64,' + file_uri;
        this.image.blob = this.b64toBlob(file_uri, 'image/JPEG');
      },
      (err) => console.log(err));
  }
  // get photo from camera
  openCamera (): void {
    const cameraOptions = {
      sourceType: Camera.PictureSourceType.CAMERA,
      destinationType: Camera.DestinationType.DATA_URL,
      encodingType: Camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      allowEdit: false,
      quality: 80,
      correctOrientation: true
    };
    Camera.getPicture(cameraOptions).then((data) => {
      this.image.b64 = 'data:image/jpeg;base64,' + data;
      this.image.blob = this.b64toBlob(data, 'image/JPEG');
    }, (err) => {
      console.log(err);
    });
  }
  // convert photo from base64 to Blob 
  b64toBlob (b64Data, contentType= '', sliceSize= 512): Blob {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  // show action choosing photo
  presentAction(): void {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Get picture',
      buttons: [
        {
          text: 'From Gallery',
          handler: () => {
            this.openGallery();
          }
        }, {
          text: 'From Camera',
          handler: () => {
            this.openCamera();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }

  // delete photo from object
  deleteUploadPhoto(): void {
    this.image.b64 = null;
    this.image.blob = null;
  }

  deletePhoto(): void {
    const alert = this.alertCtrl.create();
      alert.setTitle('Are you sure?');
      alert.setMessage('Do you want delete photo?');
      alert.addButton('Disagree');
      alert.addButton({
        text: 'Agree',
        handler: data => {
          this.deleteUploadPhoto();
        }
      });
    alert.present();
  }

  editPhoto(): void {
    new Promise((resolve, reject) => {
      if (this.image.b64) {
        const alert = this.alertCtrl.create();
          alert.setTitle('Are you sure?');
          alert.setMessage('Do you want delete photo?');
          alert.addButton({
            text: 'Disagree',
            handler: () => {
              return;
            }
          });
          alert.addButton({
            text: 'Agree',
            handler: () => {
              this.deleteUploadPhoto();
              resolve();
            }
          });
        alert.present();
      } else {
        resolve();
      }
    }).then(() => {
      this.presentAction();
    });
  }
}
