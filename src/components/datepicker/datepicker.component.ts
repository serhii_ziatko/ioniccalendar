import {
  Component,
  OnInit,
  trigger,
  animate,
  state,
  style,
  transition
} from '@angular/core';
import { NavController } from 'ionic-angular';
import * as moment from 'moment';
import { SessionServices } from '../../services/session.services';
import { CalendarServices } from '../../services/calendar.services';

@Component({
  selector: 'date-picker',
  templateUrl: 'datepicker.component.html',
  animations: [
    trigger('changedViewNext', [
      state('true' , style({opacity: 1})),
      state('false', style({opacity: 1})),
      transition('* => *', [
        style({opacity: 0, }),
        animate('1s ease', style({
            opacity: 1
        }))
      ])
    ]),
    trigger('changedViewPrev', [
        state('true' , style({width: 100})),
        state('false', style({width: 100} )),
        transition('* => *', [
            style({opacity: 0}),
            animate('1s ease', style({
                opacity: 1
            }))
        ])
    ])
  ]
})

export class DatePicker implements OnInit {
  public isChangedNext: boolean = true;
  public isChangedPrev: boolean = true;
  public isSelectedDay: Object = false;
  public threeMonthEvents: Array<App.event> = [];
  public showMonth: App.showMonth = {
    year: moment().year(),
    month: moment().month(),
    weeks: [],
    curDay: moment(),
  };
  constructor(
    private navCtrl: NavController,
    private sessionServices: SessionServices,
    private calendarServices: CalendarServices
  ) {}
  ngOnInit () {

    !this.isSelectedDay && (this.isSelectedDay = moment());

    this.calendarServices.getEventsThreeMonth({
      date: moment([this.showMonth.year, this.showMonth.month, 15]).toISOString()
    });

    this.initDays();
    this.calendarServices.events$.subscribe(events => {
      this.threeMonthEvents = events as Array<any>;
      if (moment([this.showMonth.year, this.showMonth.month]).format('MMYYYY') ===
        moment(this.isSelectedDay).format('MMYYYY')) {
        this.calendarServices.getDayEvents(this.isSelectedDay);
      }
    });
  }

  // displayed month on the template
  parseMonth(): string {
    return moment(this.showMonth.month + 1 + '').format('MMMM');
  }
  // move to previous month
  prevMonth(): void {
    this.showMonth.month -= 1;
    if (this.showMonth.month < 0) {
      this.showMonth.month = 11;
      this.showMonth.year -= 1;
    }
    this.isChangedPrev = !this.isChangedPrev;
    this.initDays();
    this.calendarServices.getEventsThreeMonth({
      date: moment([this.showMonth.year, this.showMonth.month, 15]).toISOString()
    });
  }
  // move to next month
  nextMonth(): void {
    this.showMonth.month += 1;
    if (this.showMonth.month > 11) {
      this.showMonth.month = 0;
      this.showMonth.year += 1;
    }
    this.isChangedNext = !this.isChangedNext;
    this.initDays();
    this.calendarServices.getEventsThreeMonth({
      date: moment([this.showMonth.year, this.showMonth.month, 15]).toISOString()
    });
  }
  // init calendar on page
  initDays(): void {
    // get the current year, month, first and last weeks of the current month
    const year = this.showMonth.year;
    const month = this.showMonth.month;
    let k = 0;
    const startOfMonth = moment().year(year).month(month).startOf('month');
    const endOfMonth = moment().year(year).month(month).endOf('month');
    let firstWeekOfMonth = startOfMonth.isoWeek();
    let lastWeekOfMonth = endOfMonth.isoWeek();
    this.showMonth.weeks.length = 0;
    // fix when the year starts from 52i or 53rd week
    if ( firstWeekOfMonth > lastWeekOfMonth ) {
      const lastYear = +year - 1;
      const lastWeekOfLastYear = moment().year(lastYear).endOf('year').isoWeeks();
      const lastMonthOfLastYear = 11;

      this.pushMonthToArray(firstWeekOfMonth, lastWeekOfLastYear, lastYear, lastMonthOfLastYear, k);
      firstWeekOfMonth = 1;
      k += 1;
    }
    // displayed the month 
    if ( firstWeekOfMonth < lastWeekOfMonth ) {
        this.pushMonthToArray(firstWeekOfMonth, lastWeekOfMonth, year, month, k);
    }
  }
  // make object with the days of the displayed month
  pushMonthToArray(firstWeek: number, lastWeek: number, year: number, month: number, weekIndex: number): void {
    let k = weekIndex;
    // get weeks
    for (let i = firstWeek, len = lastWeek; i <= len; i++) {
      this.showMonth.weeks.push({'weekId': i, 'days': []});
      // get days with attribute
      for (let j = 1; j < 8; j++) {
        let otherMonth = '';
        const day = moment()
          .year(year).month(month).isoWeek(i).isoWeekday(j);

        if ( i === firstWeek && +day.format('DD') > 12 ) {
          otherMonth = 'prev';
        } else if (i === lastWeek && +day.format('DD') < 21 && firstWeek !== lastWeek) {
          otherMonth = 'next';
        }

        this.showMonth.weeks[k].days.push({
          'dayId': day,
          'otherMonth': otherMonth,
        });
      }
      k++;
    }
  };

  // click event on a cell of calendar
  cellEvent(day: App.day): void {
    this.isSelectedDay = moment(day.dayId);
    day.otherMonth === 'prev' ? this.prevMonth() :
    day.otherMonth === 'next' ? this.nextMonth() : false;
    this.calendarServices.getDayEvents(this.isSelectedDay);
  }

  // filter events array for each day
  filterEvents(day: App.day): Array<App.event> {
    return this.threeMonthEvents.filter(item => {
      const cellDate = moment(day.dayId).format('DDMMYY');
      const eventDate = moment(item.date).format('DDMMYY');
      return cellDate === eventDate;
    });
  }
}
