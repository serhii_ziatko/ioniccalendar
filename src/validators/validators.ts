import { FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';

export class CustomValidators {

  public static validateEmail(c: FormControl) {
    // tslint:disable-next-line:max-line-length
    const EMAIL_REGEXP = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    return EMAIL_REGEXP.test(c.value) ? null : {
      validateEmail: {
        valid: false
      }
    };
  }

  public static validateUsername(c: FormControl) {
    return /^\d/.test(c.value) ? {
      validateUsername: {
        valid: false
      }
    } : null;
  }

  public static checkEndDate(dateKey: string, endDateKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let date = group.controls[dateKey];
      let endDate = group.controls[endDateKey];
      if (moment(endDate.value).unix() < moment(date.value).unix() ) {
        return {
          checkEndDate: true
        };
      }
      return null;
    };
  }
}
